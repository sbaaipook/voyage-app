import { Link } from 'react-router-dom';
import './single.css'

const SingleVoyage =({ image, title, id, price})=> {

  return (
    <div className='card'>
      <img className='card-img' src={image} alt={title}/>
      <div className='card-body'>
        <h1 className='card-title'>{title}</h1>
        <small className='card-price'>{price}Dh</small>
      </div>
      <Link className='card-btn' to={`info/${id}`}>plus de details</Link>
    </div>
  )
}

export default SingleVoyage;
