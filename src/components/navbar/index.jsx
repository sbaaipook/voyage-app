import './navbar.css';
import { Link } from 'react-router-dom'
import { useState } from 'react';


const Navbar =()=> {
  const [ homeActive, setHomeActive ] = useState('active')
  
  const [ aboutActive, setAboutActive ] = useState('');

  return (
    <header className='navbar'>
      <Link to={'/'} className='brand'>Voyage site</Link>

      <ul className='nav'>
        <Link to={'/'} className={`nav-item ${homeActive}`} onClick={()=> {setHomeActive('active');setAboutActive('')}}>
          <li>Acceuil</li>
        </Link>
        <Link to={'/about'} className={`nav-item ${aboutActive}`} onClick={()=>{setAboutActive('active');setHomeActive('')}}>        
          <li>About</li>
        </Link>
      </ul>
    </header>
  )
}


export default Navbar;
