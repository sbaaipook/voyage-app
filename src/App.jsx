import './assets/styles/app.css'
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom'
import Home from './pages/home'
import InfoVoyage from './pages/info-voyage'
import Navbar from './components/navbar'


const App =()=> {

  return (
    <div className="app">
      <Router>
        <Navbar /> 
        <Routes>
          <Route path='/' element={<Home/>} />
          <Route path='/info/:id' element={<InfoVoyage />} />
        </Routes>
      </Router>
    </div>
  )
}

export default App
