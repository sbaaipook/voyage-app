import SingleVoyage from '../../components/single-voyage';
import voyages from '../../assets/data/voyages'
import './home.css';

const Home =()=> {


  return (
    <div className='home'>
      <h1 className='title'>Voyages</h1>
      <div className='container'>
        {
          voyages.map(voyage=>(
            <SingleVoyage key={voyage.id} {...voyage}/>
          ))
        }
      </div>
    </div>
  )
}

export default Home;
