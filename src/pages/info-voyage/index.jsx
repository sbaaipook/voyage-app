import './info.css'
import voyages from '../../assets/data/voyages'
import { useParams } from 'react-router-dom'


const InfoVoyage =()=> {
  const { id } = useParams();
  const voyage = voyages.filter(voyage=> voyage.id == id )[0]
 
  return (
    <div className='info'>
      <img className='info-image' src={voyage.image} alt={voyage.title}/>
      <div className='info-body'>
        <h1 className='info-title'>{voyage.title}</h1>
        <small className='info-price'>{voyage.price} Dh</small>
        <p className='info-description'>
          { voyage.description }
        </p>
      </div>
      <button className='info-btn'>Apprendre</button>
    </div>
  )
}

export default InfoVoyage;
